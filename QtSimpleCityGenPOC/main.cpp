#ifndef _USE_MATH_DEFINES
    #define _USE_MATH_DEFINES
#endif // !_USE_MATH_DEFINES


#include <QtCore/QCoreApplication>
#include <iostream>
#include <chrono>
#include <random>

#include "Utils.h"
#include "cityMapVertex.h"
#include "cityMap.h"
#include "PathGenerator.h"


int main(int argc, char* argv[])
{
    /*QCoreApplication a(argc, argv);
    return a.exec();*/

    Urban::CityMap cM = Urban::CityMap();
    cM.initialiseRandomCityMap();
    /*std::cout << cM.getEnvelopeXSize() << "\t" << cM.getEnvelopeYSize() << std::endl;*/

    //for (int i = 0; i < 100; i++)
    //{
    //    //std::cout << cM.getRandomAngle_RasterAndOrganicGrowthRule(CityGen::EGrowthType::Raster) << std::endl;
    //    std::cout << cM.getRandomAngle_RasterAndOrganicGrowthRule(CityGen::EGrowthType::Raster) * 180 / M_PI << std::endl;
    //}

    //for (int i = 0; i < 100; i++)
    //{
    //    for (int j = 0; j < 100; j++)
    //    {
    //        int previousX = 0;
    //        int previousY = 0;

    //        int xTranslation = (*inputRoad)[lastIndex].xIndex - (*inputRoad)[preLastIndex].xIndex;
    //        int yTranslation = (*inputRoad)[lastIndex].yIndex - (*inputRoad)[preLastIndex].yIndex;
    //        short xDir = (short)(xTranslation / (abs(xTranslation)));
    //        short yDir = (short)(yTranslation / (abs(yTranslation)));

    //        double angle = getRandomAngle_RasterAndOrganicGrowthRule(lastVertexGrowthRule);   // <-PI; +PI>   0 - straight ahead, +PI/2 - perpendicularily counterclockwise, -PI/2 - perpendicularily clockwise,
    //        double newXDir = ((double)(xDir)) * cos(angle) - ((double)(yDir)) * sin(angle);
    //        double newYDir = ((double)(xDir)) * sin(angle) + ((double)(yDir)) * cos(angle);

    //        int newX = (int)((double)((*inputRoad)[lastIndex].xIndex) + newXDir);
    //        int newY = (int)((double)((*inputRoad)[lastIndex].yIndex) + newYDir);
    //        std::cout << newX << "\t" << newY << std::endl;


    //        if (i == 0 && j==0)
    //        {

    //        }
    //        else
    //        {
    //            if (i == 0)
    //            {

    //            }
    //            if (j == 0)
    //            {

    //            }

    //        }
    //    }
    //}

    cM.printMapDetails();

    Urban::PathGenerator patogen = Urban::PathGenerator(cM);
    //std::cout << "patogen created!" << std::endl;
    patogen.initialiseSeeds();
    //std::cout << "seeds initialised!" << std::endl;
    patogen.printSeedMap();

    //QQueue<CityGen::STwoDimIndices> seeds = patogen.getSeeds();
    ////for (int i = 0; i < seeds.count(); i++)
    //while(!seeds.isEmpty())
    //{
    //    CityGen::STwoDimIndices seed = seeds.dequeue();
    //    std::cout << seed.xIndex << "\t" << seed.yIndex << "\t\t\tremaining: " << seeds.count() << std::endl;
    //}
    patogen.generatePaths();
    patogen.printRoads();



    //if (false)
    //{
    //    long long int seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    //    std::mt19937 mtRandom1 = std::mt19937(seed);
    //    /*std::mt19937 mtRandom2(seed);
    //    for (int i = 0; i < 10; ++i)
    //    {
    //        std::cout << mtRandom1() << "\t" << mtRandom2() << std::endl;
    //    }*/
    //    QVector<QVector<int>> roads = QVector<QVector<int>>();
    //
    //    std::uniform_int_distribution<int> xDistribution = std::uniform_int_distribution<int>(1, 25);
    //    std::uniform_int_distribution<int> yDistribution = std::uniform_int_distribution<int>(1, 25);
    //
    //    for (int i = 0; i < xDistribution(mtRandom1); i++)
    //    {
    //        roads.append(QVector<int>());
    //        roads[i].clear();
    //    }
    //
    //    for (int i = 0; i < roads.count(); i++)
    //    {
    //        for (int j = 0; j < yDistribution(mtRandom1); j++)
    //        {
    //            int x = yDistribution(mtRandom1);
    //            if (!(roads[i].contains(x)))
    //            {
    //                roads[i].append(x);
    //            }
    //        }
    //    }
    //
    //    roads.append(QVector<int>());
    //    roads.last().append(1);
    //    for (int i = 0; i < 5; i++)
    //    {
    //        roads.append(QVector<int>());
    //        roads.last().append(yDistribution(mtRandom1));
    //        if ((i % 2) == 0)
    //        {
    //            roads.last().append(yDistribution(mtRandom1));
    //            roads.last().append(yDistribution(mtRandom1));
    //            roads.last().append(yDistribution(mtRandom1));
    //            roads.last().append(yDistribution(mtRandom1));
    //        }
    //    }
    //
    //    int index1 = 0;
    //    for (auto&& it1 : roads)
    //    {
    //        std::cout << "index1: " << index1 << "\tcount: " << it1.count() << std::endl << "\t\t";
    //        for (auto&& it2 : it1)
    //        {
    //            std::cout << it2 << "; ";
    //        }
    //        index1++;
    //        std::cout << std::endl;
    //    }
    //
    //    std::cout << std::endl << std::endl << std::endl;
    //
    //    /*index1 = 0;
    //    for (auto&& it1 : roads)
    //    {
    //        while(true)
    //        {
    //            std::cout << "index1: " << index1 << "\tcount: " << it1.count() << "\tcheckStatus: ";
    //            if (it1.count() <= 1 && !it1.isEmpty())
    //            {
    //                std::cout << "count IS NOT more than one" << std::endl;
    //                auto coordinatesCheck([&it1](QList<int> compare) { return compare == it1; });
    //                Utils::remove_single_if(roads, coordinatesCheck);
    //            }
    //            else
    //            {
    //                std::cout << ((!it1.isEmpty()) ? "count IS more than one" : "the list is empty") << std::endl;
    //                break;
    //            }
    //        }
    //        index1++;
    //    }*/
    //
    //    int counter = 0;
    //    for (auto&& it1 : roads)
    //    {
    //        while (true)
    //        {
    //            std::cout << "index1: " << counter << "\tcount: " << it1.count() << "\tcheckStatus: ";
    //            if (it1.count() <= 1 && !it1.isEmpty())
    //            {
    //                std::cout << "count IS NOT more than one" << std::endl;
    //                int lastIndex = roads.count() - 1;
    //
    //                if (counter < lastIndex)
    //                {
    //                    roads.swapItemsAt(counter, lastIndex);
    //                }
    //
    //                roads.removeLast();
    //            }
    //            else
    //            {
    //                std::cout << ((!it1.isEmpty()) ? "count IS more than one" : "the list is empty") << std::endl;
    //                break;
    //            }
    //        }
    //        counter++;
    //    }
    //    std::cout << std::endl << std::endl << std::endl << "Deleted!" << std::endl;
    //
    //    index1 = 0;
    //    for (auto&& it1 : roads)
    //    {
    //        std::cout << "index1: " << index1 << "\tcount: " << it1.count() << std::endl << "\t\t";
    //        for (auto&& it2 : it1)
    //        {
    //            std::cout << it2 << "; ";
    //        }
    //        index1++;
    //        std::cout << std::endl;
    //    }
    //
    //    system("cls");
    //
    //    for (int i = 0; i < 25; i++)
    //    {
    //        for (int j = 0; j < 25; j++)
    //        {
    //            if (i > 2 && i < 22 && j>2 && j < 22)
    //            {
    //                if (i % 2 == 0 && j % 3 == 0)
    //                    std::cout << "O";
    //                else
    //                    std::cout << ".";
    //            }
    //            else
    //            {
    //                std::cout << ".";
    //            }
    //
    //        }
    //        std::cout << std::endl;
    //    }
    //}
    std::cin.get();
    return 0;
}
