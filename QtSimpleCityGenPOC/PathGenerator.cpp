//#include <stdafx.h>
#include "PathGenerator.h"

using namespace Urban;

// WTF redundant initialization
// WTF addressed - swapped to default
//PathGenerator::PathGenerator()
//{
//    map = nullptr;
//    seededVertexQueue = QQueue<SVertexIndices>();
//}

PathGenerator::PathGenerator(CityMap& cM)
{
    map = &cM;
    seededVertexQueue = QQueue<SVertexIndices>();
}

// WTF redundant nullptr
// WTF addressed - destructor swapped to default
//PathGenerator::~PathGenerator()
//{
//    map = nullptr;
//    seededVertexQueue.clear();
//}

CityMap PathGenerator::getCityMap()
{
    return *(map);
}

void PathGenerator::setCityMap(CityMap& cM)
{
    map = &cM;
}

QQueue<SVertexIndices> PathGenerator::getSeeds()
{
    return seededVertexQueue;
}

void PathGenerator::initialiseSeeds()
{
    if (map->getVertices().count() < 4) //map too small
        return;

    // WTF more comments pl0x
    long long int seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 mtRandom = std::mt19937(seed);
    int seedCount = cbrt(double(map->getEnvelopeXSize()) * double(map->getEnvelopeYSize()));
    for (int i = 0; i < seedCount; i++)
    {
        std::uniform_int_distribution<int> xDistribution = std::uniform_int_distribution<int>(1, (map->getVertices().count() - 2));
        int xIndex, yCount;
        while (true)
        {
            xIndex = xDistribution(mtRandom);
            yCount = map->getVertices()[xIndex].count();
            
            if (yCount > 4)
                break;
        }

        std::uniform_int_distribution<int> yDistribution = std::uniform_int_distribution<int>(1, yCount - 2);
        int yIndex = yDistribution(mtRandom);

        map->getVertices()[xIndex][yIndex].setIsSeed(true);
        seededVertexQueue.enqueue(SVertexIndices(xIndex, yIndex));
    }
}

void PathGenerator::printSeedMap()
{
    int xBound = (getCityMap().getVertices().count() - 1);
    for (int i = 0; i < xBound; i++)
    {
        int yBound = (getCityMap().getVertices()[i].count() - 1);
        for (int j = 0; j < yBound; j++)
        {
            SVertexIndices s = SVertexIndices(i, j);
            if (getSeeds().contains(s))
            {
                std::cout << "X";
            }
            else
            {
                std::cout << ".";
            }
        }
        std::cout << std::endl;
    }
}

void PathGenerator::enqueueSeed(SVertexIndices seed)
{
    seededVertexQueue.enqueue(seed);
}

void PathGenerator::enqueueSeed(uint xIn, uint yIn)
{
    SVertexIndices seed = SVertexIndices(xIn, yIn);
    enqueueSeed(seed);
}

SVertexIndices PathGenerator::dequeueSeed()
{
    SVertexIndices result = seededVertexQueue.dequeue();
    return result;
}

CityMapVertex* PathGenerator::getDequeuedSeedVertex()
{
    SVertexIndices seedVertexIndices = dequeueSeed();
    CityMapVertex* result = &(map->getVertices()[seedVertexIndices.xIndex][seedVertexIndices.yIndex]);
    return result;
}

QVector<SVertexIndices> PathGenerator::getSuggestions(SVertexIndices vI)      //TODO
{
    return QVector<SVertexIndices>();
}

bool PathGenerator::checkIfRoadsWithinSnappingDistance(SVertexIndices vI)              //TODO
{
    bool result = false;
    for (auto&& it1 : (*map->getRoads()))
    {
        for (auto&& it2 : it1)
        {
            result = (map->getDistanceBetweenVertices(vI, it2) < SNAPPING_DISTANCE);
            if (result)
                break;
        }
        if (result)
            break;
    }
    return result;
}

int PathGenerator::findClosestRoadFromVertex(SVertexIndices vI)
{
    int closestRoadIndex = -1;
    double minDistance = DOUBLE_MAX_VALUE;
    //if (this->checkIfRoadsWithinSnappingDistance(vI))
    //{
        int counter = 0;
        for (auto&& it1 : (*map->getRoads()))
        {
            for (auto&& it2 : it1)
            {
                double distance = map->getDistanceBetweenVertices(vI, it2);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestRoadIndex = counter;
                }
            }
            counter++;
        }
    //}
    return closestRoadIndex;
}

int PathGenerator::findClosestRoadVertexIndex(SVertexIndices vI, int roadIndex)
{
    int closestVertexIndex = -1;
    double minDistance = DOUBLE_MAX_VALUE;
    QVector<SVertexIndices>* analysedRoad = &((*map->getRoads())[roadIndex]);
    int counter = 0;
    for (auto&& it1 : (*analysedRoad))
    {
        double distance = map->getDistanceBetweenVertices(vI, it1);
        if (distance < minDistance)
        {
            minDistance = distance;
            closestVertexIndex = counter;
        }
        counter++;
    }
    return closestVertexIndex;
}

std::pair<int, int> PathGenerator::findIndicesOfClosestRoadAndVertex(SVertexIndices vI)
{
    std::pair<int, int> result = std::pair<int, int>(-1, -1);

    int closestRoadIndex = -1;
    int closestVertexIndex = -1;
    double minDistance = DOUBLE_MAX_VALUE;

    int roadCounter = 0;
    for (auto&& it1 : (*map->getRoads()))
    {
        int vertexCounter = 0;
        for (auto&& it2 : it1)
        {
            double distance = map->getDistanceBetweenVertices(vI, it2);
            if (distance < minDistance)
            {
                minDistance = distance;
                closestRoadIndex = roadCounter;
                closestVertexIndex = vertexCounter;
            }
            vertexCounter++;
        }
        roadCounter++;
    }
    
    result.first = closestRoadIndex;
    result.second = closestVertexIndex;
    return result;
}

bool PathGenerator::checkIfRoadsContainerIsEmpty()
{
    bool result = map->getRoads()->isEmpty();
    //std::cout << (result ? "EMPTY" : "NOT EMPTY") << "\t" << map->getRoads()->count() << std::endl;
    return result;
}

void PathGenerator::printRoads()
{
    if (!checkIfRoadsContainerIsEmpty())
    {
        int xLimit = map->getEnvelopeXSize();
        for (int i = 0; i < xLimit; i++)
        {
            int yLimit = map->getVertices()[i].count();
            for (int j = 0; j < yLimit; j++)
            {
                char symbol = '.';
                for (auto&& it1 : (*map->getRoads()))
                {
                    SVertexIndices stdi = SVertexIndices(uint(i), uint(j));
                    //SVertexIndices suggestion = SVertexIndices(uint8_t(newXCoord), uint8_t(newYCoord));
                    auto checkIfContains([&](SVertexIndices& compare) { return (compare.xIndex == stdi.xIndex && compare.yIndex == stdi.yIndex); });
                    bool found = std::find_if(it1.begin(), it1.end(), checkIfContains) != it1.end();
                    if (found)
                        symbol = 'X';

                    //if (it1.contains(stdi))
                    //    symbol = 'X';
                }
                std::cout << symbol;
            }
            std::cout << std::endl;
        }
    }
    else
    {
        std::cout << "WHERE WE'RE GOING WE DON'T NEED ROADS!" << std::endl;
    }
}



bool PathGenerator::checkSuggestion(SVertexIndices vI)                                 //TODO
{
    bool result = false;

    long long int randomSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 mtRandom1 = std::mt19937(randomSeed);
    std::uniform_real_distribution<double> thresholdGen = std::uniform_real_distribution<double>(0.0, 100.0);
    double threshold = thresholdGen(mtRandom1);
    double val = (double(getCityMap().getPopulationDensity(vI)) * 100.0) / 256.0;
    
    result = (val > threshold);

    return result;
}

void PathGenerator::generatePaths()                                                    //TODO
{
    if (seededVertexQueue.isEmpty())
    {
        std::cout << "NO SEEDS!" << std::endl;
    }
    //else
    //{
    //    std::cout << "OK" << std::endl;
    //}

    int counterXYZ = 0;

    while (!(seededVertexQueue.isEmpty()))
    {
        counterXYZ++;
        SVertexIndices seedIndices = dequeueSeed();
        bool appendToLast = false;  
        int indexToAppend = -1;

        if (checkIfRoadsContainerIsEmpty())
        {
            //std::cout << "ROADS CONTAINER IS EMPTY" << std::endl;
            map->addNewRoad();
            //std::cout << "NEW ROAD CREATED" << std::endl;
            int index = map->getRoads()->count() - 1;
            map->appendVertexToRoad(seedIndices, index);
            //std::cout << "APPENDED SEED" << std::endl;
            
            appendToLast = true;
            indexToAppend = 0;

            //std::cout << (checkIfRoadsContainerIsEmpty() ? "ERROR" : "Fertig") << std::endl;
        }
        else
        {
            //std::cout << "ROADS CONTAINER IS NOT EMPTY" << std::endl;
            int roadIndex = -1;
            int counter = 0;
            bool inversionNecessary = false;
            //std::cout << "Beginning iteration of roadsContainer" << std::endl;
            for (auto&& it1 : (* map->getRoads()) )
            {
                //std::cout << "Iteratio:\t" << counter + 1 << std::endl;
                if ((it1.last().xIndex == seedIndices.xIndex) && (it1.last().yIndex == seedIndices.yIndex))
                {
                    roadIndex = counter;
                    appendToLast = true;
                    break;
                }
                if (it1.count() > 1)
                {
                    if ((it1.first().xIndex == seedIndices.xIndex) && (it1.first().yIndex == seedIndices.yIndex))
                    {
                        roadIndex = counter;
                        appendToLast = false;
                        inversionNecessary = true;
                        break;
                    }
                }

                counter++;
            }
            //std::cout << "Finished iteration of roadsContainer" << std::endl;

            //std::cout << "Checking if inversion is necessary" << std::endl;
            if (inversionNecessary)
            {
                //std::cout << "Inversion is necessary" << std::endl;
                QVector<SVertexIndices> invertedList = QVector<SVertexIndices>();
                //std::cout << "Inverted list created" << std::endl;
                //std::cout << "Entering inversion loop" << std::endl;
                for (QVector<SVertexIndices>::reverse_iterator rit = (*map->getRoads())[roadIndex].rbegin();
                    rit != (*map->getRoads())[roadIndex].rend();
                    ++rit)
                {
                    invertedList.append((*rit));
                }
                //std::cout << "Inversion loop executed" << std::endl;


                //map->getRoads()[roadIndex].clear();
                (*map->getRoads())[roadIndex] = invertedList;
                appendToLast = true;
                indexToAppend = roadIndex;
                //std::cout << "Inverted!" << std::endl;
            }
            
            //std::cout << "Checking if found road ending with current seed" << std::endl;
            bool foundRoadEndingWithCurrentSeed = (roadIndex >= 0);
            if (!foundRoadEndingWithCurrentSeed)
            {
                //std::cout << "Didn't find road ending with current seed" << std::endl;
                if (!(map->checkVertexIsSeed(map->getRoads()->last().last())))
                {
                    map->addNewRoad();
                    map->appendVertexToRoad(seedIndices, (map->getRoads()->count() - 1));
                    appendToLast = true;
                    indexToAppend = map->getRoads()->count() - 1;
                }
            }
            else
            {
                //std::cout << "Found road ending with current seed" << std::endl;
                indexToAppend = roadIndex;
            }
        }

        //std::cout << "Entering if" << std::endl;
        if (indexToAppend >= 0 && indexToAppend < map->getRoads()->count())
        {
            //std::cout << "If entered" << std::endl;
            QVector<SVertexIndices>* analysedRoad = &((*map->getRoads())[indexToAppend]);
            //std::cout << "x" << std::endl;
            QVector<SVertexIndices> suggestions = map->getSuggestedRoadExtensions(indexToAppend);
            //std::cout << "y" << std::endl;
            if (suggestions.count() > 0)
            {
                //std::cout << "z" << std::endl;
                for (auto&& it : suggestions)
                {
                    if (checkSuggestion(it) &&
                        (analysedRoad->isEmpty() ||
                        (analysedRoad->last().xIndex !=it.xIndex) ||
                        (analysedRoad->last().yIndex != it.yIndex))
                        )
                    {
                        //std::cout << "suggestion accepted" << std::endl;
                        //map->getVertices()[it.xIndex][it.yIndex].setIsSeed(true);
                        enqueueSeed(it);
                        analysedRoad->append(it);
                    }
                    /*
                    **else
                    **    std::cout << "suggestion denied" << std::endl;
                    */
                }
                //std::cout << "*" << std::endl;
            }
        }

        if (counterXYZ > 500)
            break;

    }

    //std::cout << "LOOP FINISHED" << std::endl;
    //FINAL STEP
    int counter = 0;
    for (auto&& it1 : (*map->getRoads()))
    {
        std::cout << "index: " << counter << "\telementCount: "<<it1.count()<<"\t";
        for (auto&& it2 : it1)
        {
            std::cout << "( " << it2.xIndex << " ; " << it2.yIndex << " )\t";
        }
        std::cout << std::endl;

        /*while (true)
        {
            if ((it1.count() <= 1) && (!(it1.isEmpty())))
            {
                auto coordinatesCheck([&it1](QVector<SVertexIndices> compare) { return ((compare == it1)); });
                Utils::remove_single_if((*map->getRoads()), coordinatesCheck);
            }
            else
            {
                break;
            }
        }*/
        counter++;
    }
}
