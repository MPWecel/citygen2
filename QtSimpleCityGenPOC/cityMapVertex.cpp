//#include <stdafx.h>
#include "cityMapVertex.h"

using namespace Urban;

//TODO - refactor, see CityMapVertex header
SVertexIndices::SVertexIndices(uint xIn, uint yIn)
{
    xIndex = xIn;
    yIndex = yIn;
}

//TODO wypierdolić
//CityMapVertex::CityMapVertex()
//{
//    x = 0.0;
//    y = 0.0;
//    neighboursList = QVector<SVertexIndices>();
//    isSeed = false;
//    roadType = ERoadType::Major;
//}
//ENDTODO

//CityMapVertex::CityMapVertex(double xIn, double yIn)
//{
//    x = xIn;
//    y = yIn;
//    neighboursList = QVector<SVertexIndices>();
//    isSeed = false;
//    roadType = ERoadType::Major;
//}

//CityMapVertex::~CityMapVertex()
//{
//}

//double CityMapVertex::getX()
//{
//    return x;
//}
//
//double CityMapVertex::getY()
//{
//    return y;
//}

//void CityMapVertex::setX(double xIn)
//{
//    x = xIn;
//}
//
//void CityMapVertex::setY(double yIn)
//{
//    y = yIn;
//}

void CityMapVertex::addNeighbour(uint xIndex, uint yIndex)
{
    neighboursList.append(SVertexIndices(xIndex, yIndex));
}

void CityMapVertex::addNeighbour(SVertexIndices neighbourCoords)
{
    /*neighboursList.append(neighbourCoords);*/
    addNeighbour(neighbourCoords.xIndex, neighbourCoords.yIndex);
}

void CityMapVertex::removeNeighbour(uint xIndex, uint yIndex)
{
    if (neighboursList.count() <= 0)
        return;

    int neighbourIndex = -1;
    for (int i = 0; i < neighboursList.count() - 1; i++)
    {
        if ((neighboursList[i].xIndex == xIndex) && 
            (neighboursList[i].yIndex == yIndex))
        {
            neighbourIndex = i;
            break;
        }
    }
    if (neighbourIndex >= 0)
    {
        neighboursList.remove(neighbourIndex);
    }
}

void CityMapVertex::removeNeighbour(SVertexIndices neighbourCoords)
{
    removeNeighbour(neighbourCoords.xIndex, neighbourCoords.yIndex);
}

QVector<SVertexIndices>* CityMapVertex::getNeighboursList()
{
    return &(neighboursList);
}

//ERoadType CityMapVertex::getRoadType()
//{
//    return roadType;
//}
//
//void CityMapVertex::setRoadType(ERoadType ertIn)
//{
//    roadType = ertIn;
//}
//
//bool CityMapVertex::IsSeed()
//{
//    return false;
//}
//
//void CityMapVertex::setIsSeed(bool sIn)
//{
//    isSeed = sIn;
//}
