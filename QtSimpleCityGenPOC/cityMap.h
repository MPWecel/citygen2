#pragma once

#define _USE_MATH_DEFINES

#include <qvector.h>
#include <chrono>
#include <random>
#include <cmath>
#include "cityMapVertex.h"
#include <iostream>

namespace Urban
{
    enum class EGrowthType
    {
        // WTF unneeded explicit assignment
        // WTF addressed
        Organic,
        Concentric,
        Raster,
        Error       //for tests only    //now also used as an undetermined state, for instance when searching for an enveloping rectangle in a jagged array and exceeding row bounds
    };

    class CityMap
    {        
    public:
        CityMap() = default;
        CityMap(bool withInitialisation) : CityMap()
        {
            if (withInitialisation)
                initialiseRandomCityMap();
        };
        ~CityMap() = default;

        // WTF British? No thank you, sir.
        // WTF denied. I refuse to speak the language of savages
        void initialiseRandomCityMap();
        void initialiseGrowthTypeArray();

        // WTF uint32_t -> uint?
        // WTF denied - change will go the other way round - uint -> uint32_t
        // N->X & M->Y change, Y->Z transition to be made later
        uint32_t getEnvelopeXSize();
        uint32_t getEnvelopeYSize();

        EGrowthType getGrowthType(SVertexIndices sIn);
        EGrowthType getGrowthType(uint xIn, uint yIn);

        // WTF return QPair<QPoint, QPoint> instead
        //TODO later
        std::pair<std::pair<int, int>,std::pair<int, int>> findGrowthTypeEnvelope(SVertexIndices sIn);
        std::pair<std::pair<int, int>, std::pair<int, int>> findGrowthTypeEnvelope(uint xIn, uint yIn);

        SVertexIndices getCircularGrowthCentre(SVertexIndices sIn);
        SVertexIndices getCircularGrowthCentre(uint xIn, uint yIn);

        uint8_t getPopulationDensity(SVertexIndices sIn);
        uint8_t getPopulationDensity(uint xIn, uint yIn);

        QVector<QVector<CityMapVertex>> getVertices();
        bool checkVertexWithinBounds(SVertexIndices sIn);
        bool checkVertexWithinBounds(uint xIn, uint yIn);
        bool checkVertexIsSeed(SVertexIndices sIn);
        bool checkVertexIsSeed(uint xIn, uint yIn);

        // WTF return const auto& instead and/or provide appropriate interface
        // WTF accepted - removing access to roads via pointer, swapping for access/modification methods //inProgress
        QVector<QVector<SVertexIndices>>* getRoads();
        void addNewRoad();
        void appendVertexToRoad(SVertexIndices vertex, int roadIndex);

        double getDistanceBetweenVertices(SVertexIndices v1, SVertexIndices v2);

        QVector<SVertexIndices> getSuggestedRoadExtensions(QVector<SVertexIndices>* inputRoad);
        QVector<SVertexIndices> getSuggestedRoadExtensions(int inputRoadIndex);

        double getRandomAngle_noConstraints();
        double getRandomAngle_RasterAndOrganicGrowthRule(EGrowthType egt);
        double getRandomAngle_Circular(EGrowthType egt, SVertexIndices stdi);

        void printMapDetails();

        // WTF data goes last
        // WTF addressed
    private:
        // WTF N = X, M = Z
        // WTF partially addressed, renaming of Y to Z to be done later alongside other similar changes
        uint32_t envelopeXSize;   // N of an N x M enveloping matrix
        uint32_t envelopeYSize;   // M of an N x M enveloping matrix
        //2D jagged arrays defining work area enveloped by the N x M matrix
        QVector<QVector<EGrowthType>> roadGrowthTypeArray;
        QVector<QVector<uint8_t>> density;
        QVector<QVector<CityMapVertex>> mapVertices;

        //array containing roads; a road is an ordered list of vertices represented by its indices in 2D space
        QVector<QVector<SVertexIndices>> roads;
    };

}
