#pragma once
#include <qvector.h>

namespace Urban
{
    enum class ERoadType
    {
        // WTF names should begin with Capital Letters
        // WTF Addressed
        Minor = 0,
        Major = 1
    };

    // WTF Replace with QPoint
    //TODO!!!
    struct SVertexIndices
    {
        uint xIndex;
        uint yIndex;
        SVertexIndices() = default;
        SVertexIndices(uint xIn, uint yIn);
        const inline bool Urban::SVertexIndices::operator==(const SVertexIndices& other)
        {
            uint otherX = other.xIndex;
            uint otherY = other.yIndex;
            bool result = ((xIndex == otherX) && (yIndex == otherY));
            return result;
        }
    };

    class CityMapVertex
    {
        public:
            CityMapVertex() = default;
            CityMapVertex(double xIn, double yIn) : x(xIn), y(yIn) {};
            ~CityMapVertex() = default;

            // WTF all getters / setters in one place, const auto& inline?
            //TODO
            const inline double& getX() const { return x; };
            const inline double& getY() const { return y; };
            void inline setX(double xIn) { x=xIn; };
            void inline setY(double yIn) { y=yIn; };

            const inline ERoadType& getRoadType() const { return roadType; };
            inline void setRoadType(ERoadType ertIn) { roadType = ertIn; };

            const inline bool IsSeed() const { return isSeed; };
            inline void setIsSeed(bool sIn) { isSeed = sIn; };

            void addNeighbour(uint xIndex, uint yIndex);
            void addNeighbour(SVertexIndices neighbourCoords);
            void removeNeighbour(uint xIndex, uint yIndex);
            void removeNeighbour(SVertexIndices neighbourCoords);
            QVector<SVertexIndices>* getNeighboursList();

        private:
            //Coordinates
            double x;
            double y;   // WTF y -> z   //TODO
            //double z;

            QVector<SVertexIndices> neighboursList = QVector<SVertexIndices>();
            ERoadType roadType = ERoadType::Major;
            bool isSeed = false;
    };
}
