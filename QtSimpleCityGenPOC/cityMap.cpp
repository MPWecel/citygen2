//#include <stdafx.h>
#include "cityMap.h"

using namespace Urban;

//CityMap::CityMap()
//{
//    // WTF use inline initializers instead AND only for nonzero values
//    // WTF addressed
//    envelopeXSize = 0;
//    envelopeYSize = 0;
//    density = QVector<QVector<uint8_t>>();
//    roadGrowthTypeArray = QVector<QVector<EGrowthType>>();
//    mapVertices = QVector<QVector<CityMapVertex>>();
//    roads = QVector<QVector<SVertexIndices>>();
//}

// WTF all objects destroy themselves when owner is destroyed - this destructor is redundant
// WTF addressed
//CityMap::~CityMap()
//{
//    density.clear();
//    roadGrowthTypeArray.clear();
//    mapVertices.clear();
//    roads.clear();
//}

void CityMap::initialiseRandomCityMap()
{
    long long int randomSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 mtRandom1 = std::mt19937(randomSeed);
    std::uniform_int_distribution<int> integerDistribution = std::uniform_int_distribution<int>(50, 100);

    envelopeXSize = integerDistribution(mtRandom1);
    density = QVector<QVector<uint8_t>>(envelopeXSize);
    roadGrowthTypeArray = QVector<QVector<EGrowthType>>(envelopeXSize);
    mapVertices = QVector<QVector<CityMapVertex>>(envelopeXSize);

    integerDistribution = std::uniform_int_distribution<int>((int(envelopeXSize * 0.90)), (int(envelopeXSize * 1.10)));
    envelopeYSize = integerDistribution(mtRandom1);

    for (int i = 0; i < envelopeXSize; i++)
    {
        if (i == 0)
            integerDistribution = std::uniform_int_distribution<int>((int(envelopeYSize * 0.90)), (int(envelopeYSize * 1.10)));
        else
            integerDistribution = std::uniform_int_distribution<int>((int(mapVertices[i - 1].count() * 0.97)), (int(mapVertices[i - 1].count()) * 1.04));

        int randomMSize = integerDistribution(mtRandom1);
        
        mapVertices[i] = QVector<CityMapVertex>(randomMSize);
        density[i] = QVector<uint8_t>(randomMSize);
        roadGrowthTypeArray[i] = QVector<EGrowthType>(randomMSize);

        //probability of road creation is quadratic based on x,y coordinates, normalised to edges of array and size of uint8_t
        double quadraticFunctionParameter = (double(envelopeXSize) / 2.0);

        double localXDensity = ((-1.0 * pow(double(i - quadraticFunctionParameter), 2) + quadraticFunctionParameter));  //Quadratic function with max at middle index and min at sides
        localXDensity += (pow(quadraticFunctionParameter, 2) - quadraticFunctionParameter); //adjust so min = 0
        localXDensity *= (256.0 / pow(quadraticFunctionParameter, 2));

        for (int j = 0; j < randomMSize; j++)
        {
            mapVertices[i][j] = CityMapVertex(double(i), double(j));

            double localYDensity = (-1.0 * pow(double(j - quadraticFunctionParameter), 2) + quadraticFunctionParameter);
            localYDensity += (pow(quadraticFunctionParameter, 2) - quadraticFunctionParameter); //adjust so min = 0
            localYDensity *= (256.0 / (pow(quadraticFunctionParameter, 2)));

            short densityXY = short(ceil((localXDensity*localYDensity)/256.0));
            density[i][j] = uint8_t(
                (densityXY < 255) ? 
                    ((densityXY > 0) ? ((uint8_t)(densityXY)) : ((uint8_t)(0))) : 
                ((uint8_t)(255)));
        }
    }
    initialiseGrowthTypeArray();
}

void CityMap::initialiseGrowthTypeArray()
{
    int growthSeedCount = ceil((envelopeXSize >= 25) ? (cbrt(ceil(envelopeXSize * envelopeYSize))) : (sqrt(ceil(envelopeXSize * envelopeYSize))));
    long long int randomSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 mtRandom1 = std::mt19937(randomSeed);
    std::uniform_int_distribution<int> integerDistribution = std::uniform_int_distribution<int>(1, int(ceil(0.8*growthSeedCount)));

    int circularSeedCount = integerDistribution(mtRandom1);
    int rasterSeedCount = (circularSeedCount < growthSeedCount) ? (growthSeedCount - circularSeedCount) : (0);

    integerDistribution = std::uniform_int_distribution<int>(0, std::max(envelopeXSize, envelopeYSize));
    QVector<SVertexIndices> growthSeeds = QVector<SVertexIndices>(growthSeedCount);
    QVector<QVector<bool>> vertexChecked = QVector<QVector<bool>>(envelopeXSize);

    for (int i = 0; i < envelopeXSize; i++)
    {
        int ySize = mapVertices[i].count() - 1;
        vertexChecked[i] = QVector<bool>(ySize);
        for (int j = 0; j < ySize; j++)
        {
            vertexChecked[i][j] = false;
        }
    }
    
    for (int i = 0; i < growthSeedCount; i++)
    {
        uint8_t xCoord = uint8_t(integerDistribution(mtRandom1));
        uint8_t yCoord = uint8_t(integerDistribution(mtRandom1));
        while (true)
        {
            bool check = checkVertexWithinBounds(xCoord, yCoord);
            if (check)
                break;
            else
            {
                xCoord = uint8_t(integerDistribution(mtRandom1));
                yCoord = uint8_t(integerDistribution(mtRandom1));
            }
        }
        EGrowthType insertedGrowthType = (i < circularSeedCount) ? (EGrowthType::Concentric) : (EGrowthType::Raster);
        roadGrowthTypeArray[xCoord][yCoord] = insertedGrowthType;
        growthSeeds[i] = SVertexIndices(xCoord, yCoord);
        vertexChecked[xCoord][yCoord] = true;
    }

    // WTF formatting pl0x
    // WTF addressed formatting 
    int maxAreaRadius = ((envelopeXSize >= 50) ? int(0.8 * sqrt(envelopeXSize)) :
                                                 ((envelopeXSize >= 10) ? int(0.8 * pow(envelopeXSize, 0.75)) : (3))
                        );

    for (int i = 1; i < maxAreaRadius; i++)
    {
        for (int j = 0; j < growthSeedCount; j++)
        {
            int xCoord = growthSeeds[j].xIndex;
            int yCoord = growthSeeds[j].yIndex;
            EGrowthType growthTypeToInsert = roadGrowthTypeArray[xCoord][yCoord];

            int xMin = ((xCoord - i) > 0) ? (xCoord - i) : 0;
            int xMax = ((xCoord + i) < (envelopeXSize-2) ) ? (xCoord + i) : (envelopeXSize - 2);

            if (xMin >= xMax)
                continue;

            for (int a = xMin; a <= xMax; a++)
            {
                int yBoundary = (mapVertices[a].count() - 1);
                int yMin = ((yCoord - i) > 0) ? (yCoord - i) : 0;
                int yMax = ((yCoord + i) < (yBoundary)) ? (yCoord + i) : (yBoundary-1);

                if (yMin >= yMax)
                    continue;

                for (int b = yMin; b <= yMax; b++)
                {
                    bool isCorner = (
                        (a == xMin || a == xMax || a == xMin + 1 || a == xMax - 1) && (b == yMin || b == yMax) ||
                        (a == xMin || a == xMax) && (b == yMin || b == yMax || b == yMin + 1 || b == yMax - 1)
                        );
                    if (!vertexChecked[a][b] && !isCorner)
                    {
                        roadGrowthTypeArray[a][b] = growthTypeToInsert;
                        vertexChecked[a][b] = true;
                    }
                }
            }
        }
    }

    for (int i = 0; i < envelopeXSize-1; i++)
    {
        int maxY = (mapVertices[i].count() - 1);
        for (int j = 0; j < maxY; j++)
        {
            if (!(vertexChecked[i][j]))
            {
                roadGrowthTypeArray[i][j] = EGrowthType::Organic;
            }
        }
    }

}

uint32_t CityMap::getEnvelopeXSize()
{
    return envelopeXSize;
}

uint32_t CityMap::getEnvelopeYSize()
{
    return envelopeYSize;
}

EGrowthType CityMap::getGrowthType(SVertexIndices sIn)
{
    //EGrowthType result = roadGrowthTypeArray[sIn.xIndex][sIn.yIndex];
    return getGrowthType(sIn.xIndex, sIn.yIndex);
}

EGrowthType CityMap::getGrowthType(uint xIn, uint yIn)
{
    //std::cout << "getGrowthType\t" << "Entering" << std::endl;
    //std::cout << "inputParametres\t" << "x: " << xIn << " y: " << yIn << std::endl;
    //std::cout << "arraySize\t" << roadGrowthTypeArray.count() << " " << ((xIn < roadGrowthTypeArray.count()) ? (roadGrowthTypeArray[xIn].count()) : (-1)) << std::endl;
    bool isWithinBounds = checkVertexWithinBounds(xIn, yIn);
    //std::cout << "withinBounds: " << (isWithinBounds ? ("true") : ("false")) << std::endl;
    EGrowthType result = isWithinBounds ? roadGrowthTypeArray[xIn][yIn] : EGrowthType::Error;
    return result;
}

std::pair<std::pair<int, int>, std::pair<int, int>> CityMap::findGrowthTypeEnvelope(SVertexIndices sIn)
{
    //std::cout << "findGrowthTypeEnvelope\t" << "entered" << std::endl;

    int xLeft = INT16_MAX;
    int xRight = INT16_MIN;
    int yTop = INT16_MAX;
    int yBottom = INT16_MIN;

    //std::cout << "findGrowthTypeEnvelope\t" << "gettingGrowthType" << std::endl;
    // WTF more comments pl0x
    EGrowthType localGrowthType = getGrowthType(sIn);
    //std::cout << "findGrowthTypeEnvelope\t" << "got Growth Type" << std::endl;
    int xRange = std::max((getEnvelopeXSize() - uint32_t(sIn.xIndex)), uint32_t(sIn.xIndex));
    int yRange = std::max((getEnvelopeYSize() - uint32_t(sIn.yIndex)), uint32_t(sIn.yIndex));

    //std::cout << "findGrowthTypeEnvelope\t" << "entering loop 1" << std::endl;
    for (int xMod = 0; xMod < xRange; xMod++)
    {
        int newXLeft  = (((int(sIn.xIndex) - xMod) >= 0) ? (int(sIn.xIndex) - xMod) : (0));
        int newXRight = (((int(sIn.xIndex) + xMod) < (mapVertices.count() - 1)) ? (int(sIn.xIndex) + xMod) : (mapVertices.count() - 1));

        int yLeftRange = std::max((mapVertices[newXLeft].count() - int(sIn.yIndex)), int(sIn.yIndex));
        int yRightRange = std::max((mapVertices[newXRight].count() - int(sIn.yIndex)), int(sIn.yIndex));

        //std::cout << "findGrowthTypeEnvelope\t" << "entering loop 2" << std::endl;
        for (int yMod = 0; yMod < yLeftRange; yMod++)
        {
            //std::cout << "findGrowthTypeEnvelope\t" << "loop 2" <<"\tgetNewYTop" << std::endl;
            int newYTop = (((int(sIn.yIndex) - yMod) >= 0) ? (int(sIn.yIndex) - yMod) : (0));
            //std::cout << "findGrowthTypeEnvelope\t" << "loop 2" << "\tgetNewYBottom" << std::endl;
            int newYBottom = (((int(sIn.yIndex) + yMod) < (mapVertices[newXLeft].count() - 1)) ? (int(sIn.xIndex) + xMod) : (mapVertices[newXLeft].count() - 1));
            //std::cout << "findGrowthTypeEnvelope\t" << "loop 2" << "\tcheckIfWithinGrowthType" << std::endl;
            if (!(getGrowthType(newXLeft, newYTop) == localGrowthType) && !(getGrowthType(newXLeft, newYBottom) == localGrowthType))
            {
                break;
            }
            else
            {
                if (getGrowthType(newXLeft, newYTop) == localGrowthType)
                {
                    xLeft = std::min(xLeft, newXLeft);
                    yTop = std::min(yTop, newYTop);
                }
                if (getGrowthType(newXLeft, newYBottom) == localGrowthType)
                {
                    xLeft = std::min(xLeft, newXLeft);
                    yBottom = std::max(yBottom, newYBottom);
                }
            }
            
        }
        //std::cout << "findGrowthTypeEnvelope\t" << "exited loop 2" << std::endl;
        //std::cout << "findGrowthTypeEnvelope\t" << "entering loop 3" << std::endl;
        for (int yMod = 0; yMod < yRightRange; yMod++)
        {
            //std::cout << "findGrowthTypeEnvelope\t" << "loop 3" << "\t" << yMod << std::endl;
            //std::cout << "findGrowthTypeEnvelope\t" << "loop 3" << "\tgetNewYTop" << std::endl;
            int newYTop = (((int(sIn.yIndex) - yMod) >= 0) ? (int(sIn.yIndex) - yMod) : (0));
            //std::cout << "findGrowthTypeEnvelope\t" << "loop 3" << "\tgetNewYBottom" << std::endl;
            int newYBottom = (((int(sIn.yIndex + yMod)) < (mapVertices[newXRight].count() - 1)) ? (int(sIn.xIndex) + xMod) : (mapVertices[newXRight].count() - 1));
            //std::cout << "findGrowthTypeEnvelope\t" << "loop 3" << "\tcheckIfWithinGrowthType" << std::endl;
            if (!(getGrowthType(newXRight, newYTop) == localGrowthType) && !(getGrowthType(newXRight, newYBottom) == localGrowthType))
            {
                break;
            }
            else
            {
                if ((getGrowthType(newXRight, newYTop) == localGrowthType))
                {
                    xRight = std::max(xRight, newXRight);
                    yTop = std::min(yTop, newYTop);
                }
                if (getGrowthType(newXRight, newYBottom) == localGrowthType)
                {
                    xRight = std::max(xRight, newXRight);
                    yBottom = std::max(yBottom, newYBottom);
                }
            }
        }
        //std::cout << "findGrowthTypeEnvelope\t" << "exited loop 3" << std::endl;
    }
    //std::cout << "findGrowthTypeEnvelope\t" << "exited loop 1" << std::endl;
    
    std::pair<std::pair<int, int>, std::pair<int, int>> result = std::pair<std::pair<int, int>, std::pair<int, int>>();
    result.first.first = xLeft;
    result.first.second = xRight;
    result.second.first = yTop;
    result.second.second = yBottom;
    //std::cout << "findGrowthTypeEnvelope\t" << "exited" << std::endl;
    return result;
}

std::pair<std::pair<int, int>, std::pair<int, int>> CityMap::findGrowthTypeEnvelope(uint xIn, uint yIn)
{
    SVertexIndices analysedVertexCoords = SVertexIndices(xIn, yIn);
    return findGrowthTypeEnvelope(analysedVertexCoords);
}

//TODO total overhaul
SVertexIndices CityMap::getCircularGrowthCentre(SVertexIndices sIn)
{
    std::pair<std::pair<int, int>, std::pair<int, int>> envelope = findGrowthTypeEnvelope(sIn);
    int xIndex = envelope.first.second - envelope.first.first;
    int yIndex = envelope.second.second - envelope.second.first;
    
    SVertexIndices result = SVertexIndices(xIndex, yIndex);

    return result;
}

SVertexIndices CityMap::getCircularGrowthCentre(uint xIn, uint yIn)
{
    SVertexIndices analysedVertexCoords = SVertexIndices(xIn, yIn);
    return getCircularGrowthCentre(analysedVertexCoords);
}

uint8_t CityMap::getPopulationDensity(SVertexIndices sIn)
{
    return getPopulationDensity(sIn.xIndex, sIn.yIndex);
}

uint8_t CityMap::getPopulationDensity(uint xIn, uint yIn)
{
    uint8_t result = 0;
    
    if (xIn < getEnvelopeXSize())
        if (yIn < density[xIn].count())
            result = density[xIn][yIn];

    return result;
}

QVector<QVector<CityMapVertex>> CityMap::getVertices()
{
    return mapVertices;
}

bool CityMap::checkVertexWithinBounds(SVertexIndices sIn)
{
    bool result = (((mapVertices.count() - 1) > sIn.xIndex) && ((mapVertices[sIn.xIndex].count()-1) > sIn.yIndex));
    return result;
}

bool CityMap::checkVertexWithinBounds(uint xIn, uint yIn)
{
    bool result = (((mapVertices.count()-1) > xIn) && ((mapVertices[xIn].count()-1) > yIn));
    return result;
}

bool CityMap::checkVertexIsSeed(SVertexIndices sIn)
{
    bool result = false;
    if (checkVertexWithinBounds(sIn))
    {
        result = mapVertices[sIn.xIndex][sIn.yIndex].IsSeed();
    }
    return false;
}

bool CityMap::checkVertexIsSeed(uint xIn, uint yIn)
{
    SVertexIndices analysedVertex = SVertexIndices(xIn, yIn);
    return checkVertexIsSeed(analysedVertex);
}

QVector<QVector<SVertexIndices>>* CityMap::getRoads()
{
    return &roads;
}

void CityMap::addNewRoad()
{
    QVector<SVertexIndices> newRoad = QVector<SVertexIndices>();
    roads.append(newRoad);
}

void CityMap::appendVertexToRoad(SVertexIndices vertex, int roadIndex)
{
    if (roads.count() > roadIndex)
    {
        roads[roadIndex].append(vertex);
    }
}

double CityMap::getDistanceBetweenVertices(SVertexIndices v1, SVertexIndices v2)
{
    double result = std::sqrt(double((v2.xIndex - v1.xIndex)^2 + (v2.yIndex - v1.yIndex)^2));
    return result;
}

QVector<SVertexIndices> CityMap::getSuggestedRoadExtensions(QVector<SVertexIndices>* inputRoad)
{
    QVector<SVertexIndices> result = QVector<SVertexIndices>();

    if (inputRoad == nullptr)
        return result;

    int roadVertexCount = inputRoad->count();   // poprawi� na strza�ki dalej done
    
    if (roadVertexCount <= 0)
        return result;

    EGrowthType lastVertexGrowthRule = getGrowthType(inputRoad->last());
    uint8_t lastVertexPopulationDensity = getPopulationDensity(inputRoad->last());

    if (roadVertexCount == 1)
    {
        int numberOfSuggestions = (getPopulationDensity(inputRoad->last().xIndex, inputRoad->last().yIndex) < 127) ? 1 : 
            ((getPopulationDensity(inputRoad->last().xIndex, inputRoad->last().yIndex) < 192) ? 2 : 3);

        for (int i = 0; i < numberOfSuggestions; i++)
        {
            double angle = getRandomAngle_noConstraints();    // angles defined within range <-PI; +PI> 0 - straight ahead, +PI/2 - perpendicularily counterclockwise, -PI/2 - perpendicularily clockwise; 
                                                //no constraints put on angle - equal likelihood of any angle to be output
            double newXVersor = cos(angle);     //default (1,0) versor used as input, then rotated using angle - in this case - input is simplified
            double newYVersor = sin(angle);     //
            double pathLength = sqrt(2.0);      //max leap - diagonally by one vertex   temporary feature, to be replaced by a more sophisticated solution

            double aux = round(double(inputRoad->last().xIndex) + (newXVersor * pathLength));

            int newXCoord = ((aux > 0) && (aux < getEnvelopeXSize())) ? int(aux) : int((aux < 0) ? 0 : (getEnvelopeXSize() - 1));
            aux = round(double((*inputRoad).last().yIndex) + (newYVersor * pathLength));
            int newYCoord = ((aux > 0) && (aux < getVertices()[newXCoord].count())) ? int(aux) : int((aux < 0) ? 0 : (getVertices()[newXCoord].count() - 1));

            if (checkVertexWithinBounds(uint(newXCoord), uint(newYCoord)))
            {
                SVertexIndices suggestion = SVertexIndices(uint8_t(newXCoord), uint8_t(newYCoord));
                auto checkIfContains([&](SVertexIndices& compare) { return (compare.xIndex == suggestion.xIndex && compare.yIndex == suggestion.yIndex); });
                bool found = std::find_if(result.begin(), result.end(), checkIfContains) != result.end();
                if (!found)
                    result.append(suggestion);
            }
        }
    }
    if (roadVertexCount > 1)
    {
        int preLastIndex = inputRoad->count() - 2;
        int lastIndex = inputRoad->count() - 1;

        if ((inputRoad->at(preLastIndex).xIndex == inputRoad->at(lastIndex).xIndex) &&
            (inputRoad->at(preLastIndex).yIndex == inputRoad->at(lastIndex).yIndex) &&
            (roadVertexCount > 2))
        {
            int preLastIndex = inputRoad->count() - 3;
        }

        int xTranslation = (*inputRoad)[lastIndex].xIndex - (*inputRoad)[preLastIndex].xIndex;
        int yTranslation = (*inputRoad)[lastIndex].yIndex - (*inputRoad)[preLastIndex].yIndex;
        short xDir = short(xTranslation / (abs(xTranslation)));
        short yDir = short(yTranslation / (abs(yTranslation)));

        int numberOfSuggestions = 0;
        numberOfSuggestions = (getPopulationDensity(inputRoad->last().xIndex, inputRoad->last().yIndex) < 32) ? 0 :
            ((getPopulationDensity(inputRoad->last().xIndex, inputRoad->last().yIndex) < 128) ? 1 : 
            ((getPopulationDensity(inputRoad->last().xIndex, inputRoad->last().yIndex) < 192) ? 2 : 3));

        if (numberOfSuggestions > 0)
        {
            for (int i = 0; i < numberOfSuggestions; i++)
            {
                // angles defined within range <-PI; +PI>, generator restricts it around expected values   
                //0 - straight ahead, +PI/2 - perpendicularily counterclockwise, -PI/2 - perpendicularily clockwise,
                /*double angle = (lastVertexGrowthRule != EGrowthType::Concentric) ? getRandomAngle_RasterAndOrganicGrowthRule(lastVertexGrowthRule) : getRandomAngle_Circular(lastVertexGrowthRule, inputRoad->last());*/
                double angle = getRandomAngle_RasterAndOrganicGrowthRule(lastVertexGrowthRule);
                if (lastVertexGrowthRule == EGrowthType::Concentric)
                {
                    SVertexIndices axis = getCircularGrowthCentre(inputRoad->last());
                    double radius = getDistanceBetweenVertices(inputRoad->last(), axis);
                    double maxLeap = sqrt(2);
                    angle = std::acos(maxLeap / radius);
                    bool isAnglePositive = false;   //TODO check!!!
                    if (axis.xIndex == inputRoad->last().xIndex)
                    {
                        if (axis.yIndex < inputRoad->last().yIndex)
                            isAnglePositive = (yDir < 0);
                        else
                            isAnglePositive = (yDir > 0);
                    }
                    else
                    {
                        if (axis.xIndex < inputRoad->last().xIndex)
                            isAnglePositive = (xDir > 0);
                        else
                            isAnglePositive = (xDir < 0);
                    }
                }

                double newXDir = (double(xDir) * cos(angle)) - (double(yDir) * sin(angle));
                double newYDir = (double(xDir) * sin(angle)) + (double(yDir) * cos(angle));

                int newX = round(((*inputRoad)[lastIndex].xIndex) + newXDir);
                int newY = round(((*inputRoad)[lastIndex].yIndex) + newYDir);
                if (checkVertexWithinBounds(uint(newX), uint(newY)))
                {
                    SVertexIndices suggestion = SVertexIndices(uint8_t(newX), uint8_t(newY));
                    auto checkIfContains([&](SVertexIndices& compare) { return (compare.xIndex == suggestion.xIndex && compare.yIndex == suggestion.yIndex); });
                    bool found = std::find_if(result.begin(), result.end(), checkIfContains) != result.end();
                    if (!found)
                        result.append(suggestion);
                }
            }
        }
    }

    return result;
}

// WTF remove duplicate method
// access method will be changed to use index instead of pointer
QVector<SVertexIndices> CityMap::getSuggestedRoadExtensions(int inputRoadIndex)
{
    QVector<SVertexIndices> result = QVector<SVertexIndices>();

    if (inputRoadIndex >= roads.count())
        return result;

    int roadVertexCount = roads[inputRoadIndex].count();   

    if (roadVertexCount <= 0)
        return result;

    QVector<SVertexIndices>* roadPointer = &(roads[inputRoadIndex]);

    EGrowthType lastVertexGrowthRule = getGrowthType(roadPointer->last());
    uint8_t lastVertexPopulationDensity = getPopulationDensity(roadPointer->last());

    if (roadVertexCount == 1)
    {
        int numberOfSuggestions = (getPopulationDensity(roadPointer->last().xIndex, roadPointer->last().yIndex) < 127) ? 1 :
            ((getPopulationDensity(roadPointer->last().xIndex, roadPointer->last().yIndex) < 192) ? 2 : 3);

        for (int i = 0; i < numberOfSuggestions; i++)
        {
            double angle = getRandomAngle_noConstraints();    // angles defined within range <-PI; +PI> 0 - straight ahead, +PI/2 - perpendicularily counterclockwise, -PI/2 - perpendicularily clockwise; 
                                                // no constraints put on angle - equal likelihood of any angle to be output
            double newXVersor = cos(angle);     // default (1,0) versor used as input, then rotated using angle - in this case - input is simplified
            double newYVersor = sin(angle);     //
            double pathLength = sqrt(2.0);      //max leap - diagonally by one vertex   temporary feature, to be replaced by a more sophisticated solution

            double aux = round(double(roadPointer->last().xIndex) + (newXVersor * pathLength));

            int newXCoord = ((aux > 0) && (aux < getEnvelopeXSize())) ? int(aux) : int((aux < 0) ? 0 : (getEnvelopeXSize() - 1));
            aux = round((double)(roadPointer->last().yIndex) + (newYVersor * pathLength));
            int newYCoord = ((aux > 0) && (aux < getVertices()[newXCoord].count())) ? int(aux) : int((aux < 0) ? 0 : (getVertices()[newXCoord].count() - 1));

            if (checkVertexWithinBounds(uint(newXCoord), uint(newYCoord)))
            {
                SVertexIndices suggestion = SVertexIndices(uint8_t(newXCoord), uint8_t(newYCoord));
                auto checkIfContains([&](SVertexIndices& compare) { return (compare.xIndex == suggestion.xIndex && compare.yIndex == suggestion.yIndex); });
                bool found = std::find_if(result.begin(), result.end(), checkIfContains) != result.end();
                if (!found)
                    result.append(suggestion);
            }
        }
    }
    if (roadVertexCount > 1)
    {
        //std::cout << "getSuggestedRoadExtensions\t" << "if2" << std::endl;
        int preLastIndex = roadPointer->count() - 2;
        int lastIndex = roadPointer->count() - 1;
        //std::cout << "getSuggestedRoadExtensions\t" << "if2" << "\t1 indicesSet" << std::endl;
        if ((roadPointer->at(preLastIndex).xIndex == roadPointer->at(lastIndex).xIndex) && 
            (roadPointer->at(preLastIndex).yIndex == roadPointer->at(lastIndex).yIndex) &&
            (roadVertexCount > 2))
        {
            //std::cout << "getSuggestedRoadExtensions\t" << "if2" << "\t1a indicesIdentical changing" << std::endl;
            int preLastIndex = roadPointer->count() - 3;
        }
        //std::cout << "getSuggestedRoadExtensions\t" << "if2" << "\t2 indicesChanged" << std::endl;

        int xTranslation = (*roadPointer)[lastIndex].xIndex - (*roadPointer)[preLastIndex].xIndex;
        int yTranslation = (*roadPointer)[lastIndex].yIndex - (*roadPointer)[preLastIndex].yIndex;

        //std::cout << "getSuggestedRoadExtensions\t" << "if2" << "\t3 got translation" << std::endl;

        short xDir = (xTranslation > 0) ? short(xTranslation / (abs(xTranslation))) : (0);
        short yDir = (yTranslation > 0) ? short(yTranslation / (abs(yTranslation))) : (0);

        //std::cout << "getSuggestedRoadExtensions\t" << "if2" << "\t4 got versor" << std::endl;

        int numberOfSuggestions = 0;
        numberOfSuggestions = (getPopulationDensity(roadPointer->last().xIndex, roadPointer->last().yIndex) < 32) ? 0 :
            ((getPopulationDensity(roadPointer->last().xIndex, roadPointer->last().yIndex) < 128) ? 1 :
            ((getPopulationDensity(roadPointer->last().xIndex, roadPointer->last().yIndex) < 192) ? 2 : 3));

        //std::cout << "getSuggestedRoadExtensions\t" << "if2" << "\t5 got number of suggestions" << std::endl;

        //std::cout << "getSuggestedRoadExtensions\t" << "if3" << "\tentering if" << std::endl;
        if (numberOfSuggestions > 0)
        {
            //std::cout << "getSuggestedRoadExtensions\t" << "if3" << "\t1 entering loop" << std::endl;
            for (int i = 0; i < numberOfSuggestions; i++)
            {
                // angles defined within range <-PI; +PI>, generator restricts it around expected values   
                //0 - straight ahead, +PI/2 - perpendicularily counterclockwise, -PI/2 - perpendicularily clockwise,
                /*double angle = (lastVertexGrowthRule != EGrowthType::Concentric) ? getRandomAngle(lastVertexGrowthRule) : getRandomAngle_RasterAndOrganicGrowthRule(lastVertexGrowthRule, inputRoad->last());*/
                double angle = getRandomAngle_RasterAndOrganicGrowthRule(lastVertexGrowthRule);
                if (lastVertexGrowthRule == EGrowthType::Concentric)
                {
                    //std::cout << "chuj1" << std::endl;
                    if (roadPointer->isEmpty())
                    {
                        //std::cout << "Droga, kurwa, pusta!" << std::endl;
                    }
                    else
                    {
                        //std::cout << "Droga jest" << std::endl;
                        /*std::cout << roadPointer->at(roadPointer->count() - 1).xIndex << "\t" << roadPointer->at(roadPointer->count() - 1).yIndex << std::endl;*/
                        //std::cout << roadPointer->last().xIndex << "\t" << roadPointer->last().yIndex << std::endl;
                    }

                    SVertexIndices axis = getCircularGrowthCentre(roadPointer->last());
                    //std::cout << "chuj2" << std::endl;
                    double radius = getDistanceBetweenVertices(roadPointer->last(), axis);
                    //std::cout << "chuj3" << std::endl;
                    double maxLeap = sqrt(2);
                    angle = std::acos(maxLeap / radius);
                    bool isAnglePositive = false;   //TODO check!!!
                    //std::cout << "chuj4" << std::endl;
                    if (axis.xIndex == roadPointer->last().xIndex)
                    {
                        //std::cout << "chuj5" << std::endl;
                        if (axis.yIndex < roadPointer->last().yIndex)
                            isAnglePositive = (yDir < 0);
                        else
                            isAnglePositive = (yDir > 0);
                        //std::cout << "chuj6" << std::endl;
                    }
                    else
                    {
                        //std::cout << "chuj7" << std::endl;
                        if (axis.xIndex < roadPointer->last().xIndex)
                            isAnglePositive = (xDir > 0);
                        else
                            isAnglePositive = (xDir < 0);
                        //std::cout << "chuj8" << std::endl;
                    }
                    //std::cout << "chuj9" << std::endl;
                }
                //std::cout << "chuj10" << std::endl;

                double newXDir = (double(xDir) * cos(angle)) - (double(yDir) * sin(angle));
                double newYDir = (double(xDir) * sin(angle)) + (double(yDir) * cos(angle));

                //std::cout << "getSuggestedRoadExtensions\t" << "if3" << "\tloop1" << "\t1assigningNewCoords" << std::endl;

                int newX = round(((*roadPointer)[lastIndex].xIndex) + newXDir);
                int newY = round(((*roadPointer)[lastIndex].yIndex) + newYDir);

                //std::cout << "getSuggestedRoadExtensions\t" << "if3" << "\tloop1" << "\t2NewCoords assigned" << std::endl;
                //std::cout << newX << "\t" << newY << std::endl;
                if (checkVertexWithinBounds(uint(newX), uint(newY)))
                {
                    SVertexIndices suggestion = SVertexIndices(uint8_t(newX), uint8_t(newY));
                    auto checkIfContains([&](SVertexIndices& compare) { return (compare.xIndex == suggestion.xIndex && compare.yIndex == suggestion.yIndex); });
                    bool found = std::find_if(result.begin(), result.end(), checkIfContains) != result.end();
                    if (!found)
                        result.append(suggestion);
                }
            }
            //std::cout << "getSuggestedRoadExtensions\t" << "if3" << "\t2 exited loop" << std::endl;
        }
    }
    //std::cout << "getSuggestedRoadExtensions\t" << "endif2" << std::endl;

    return result;
}
// WTF indicate contraints in method name
// WTF addressed
double CityMap::getRandomAngle_noConstraints()
{
    long long int randomSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 mtRandom1 = std::mt19937(randomSeed);
    std::uniform_real_distribution<double> realDistribution = std::uniform_real_distribution<double>(((-1)*(M_PI)), (M_PI));

    return realDistribution(mtRandom1);
}
// WTF indicate contraints in method name
// WTF addressed
double CityMap::getRandomAngle_RasterAndOrganicGrowthRule(EGrowthType egt)
{
    double result = 0.0;
    long long int randomSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 mtRandom1 = std::mt19937(randomSeed);

    switch (egt)
    {
        case EGrowthType::Raster:
        {
            std::vector<int> values { -1, 0, 1, 2 };
            std::vector<double> weights{ 2.0, 3.0, 2.0, 1.0 };
            std::piecewise_linear_distribution<double> quarterSelection = std::piecewise_linear_distribution<double>(values.begin(), values.end(), weights.begin());  //selected angle will be rotated -1/0/1/2 times 90 degrees to accomodate all possible perpendicular ranges
            
            std::normal_distribution<double> angleAround0 = std::normal_distribution<double>(0.0, (M_PI / 36)); //standard deviation 5 degrees, 70% of angles will fall within <-5;5> degrees range
            
            double angle = angleAround0(mtRandom1);
            int quarter = int(round(quarterSelection(mtRandom1)));
            result = angle + double(M_PI_2 * quarter);
            break;
        }
        case EGrowthType::Organic:
        {
            std::normal_distribution<double> angleAround30 = std::normal_distribution<double>(M_PI/6, (M_PI / 12)); // mean val 30 degrees standard deviation 15 degrees, 70% of angles will fall within <15;45> degrees range
            std::uniform_int_distribution<int> quarterSelection = std::uniform_int_distribution<int>(0, 1);    //selected angle will be rotated -1/0/1/2 times 90 degrees to accomodate all possible perpendicular ranges
            double angle = angleAround30(mtRandom1);
            int quarter = quarterSelection(mtRandom1);
            result = angle * ((quarter == 0) ? (1) : (-1));

            break;
        }
        case EGrowthType::Concentric:
        {
            //UNDEFINED
        }
        default:
        {
            //UNDEFINED
        }
    }

    return result;
}
// WTF indicate contraints in method name
// WTF addressed
double CityMap::getRandomAngle_Circular(EGrowthType egt, SVertexIndices stdi)
{
    double result = 0.0;
    
    switch (egt)
    {
        case EGrowthType::Raster:
        {
            result = getRandomAngle_RasterAndOrganicGrowthRule(egt);
            break;
        }
        case EGrowthType::Organic:
        {
            result = getRandomAngle_RasterAndOrganicGrowthRule(egt);
            break;
        }
        case EGrowthType::Concentric:
        {
            long long int randomSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            std::mt19937 mtRandom1 = std::mt19937(randomSeed);
            SVertexIndices axis = getCircularGrowthCentre(stdi);
            double radius = getDistanceBetweenVertices(stdi, axis);
            double maxLeap = sqrt(2);
            double angle = std::acos(maxLeap / radius);

        }
        default:
        {
            //UNDEFINED
        }
    }

    return result;
}

void CityMap::printMapDetails()
{
    for (int i = 0; i < getVertices().count()-1; i++)
    {
        int jMax = (getVertices()[i].count() - 1);
        for (int j = 0; j < jMax; j++)
        {
            std::string rgt;
            switch (roadGrowthTypeArray[i][j])
            {
                case EGrowthType::Concentric:
                {
                    rgt = "C";
                    break;
                }
                case EGrowthType::Organic:
                {
                    rgt = "O";
                    break;
                }
                case EGrowthType::Raster:
                {
                    rgt = "R";
                    break;
                }
                case EGrowthType::Error:
                {
                    rgt = ".";
                    break;
                }
                default:
                {
                    rgt = ".";
                    break;
                }
            }
            
            std::cout << rgt;
        }
        std::cout << std::endl;
    }

    std::cout << std::endl << std::endl;

    for (int i = 0; i < getVertices().count() - 1; i++)
    {
        int jMax = (getVertices()[i].count() - 1);
        for (int j = 0; j < jMax; j++)
        {
            std::string densString;
            double densDouble = (((double)(density[i][j])) / 256.0) * 100;

            densString = ((densDouble < 10) ? (".") : 
                   ((densDouble < 25) ? ("o") : 
                   ((densDouble < 50) ? ("x") : 
                   ((densDouble < 75) ? ("X") : ("0")
                   ))));

            std::cout << densString;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl << std::endl;
}
