#pragma once
namespace Utils
{
    template<typename T>
    bool remove_single(T& v, const typename T::value_type& e)
    {
        auto it = std::find(v.begin(), v.end(), e);
        if (it == v.end())
            return false;

        *it = std::move(v.back());
        v.pop_back();
        return true;
    }

    template<typename T, typename Pred>
    bool remove_single_if(T& v, Pred pred)
    {
        auto it = std::find_if(v.begin(), v.end(), pred);
        if (it == v.end())
            return false;

        *it = std::move(v.back());
        v.pop_back();
        return true;
    }
    
    /*template<typename T>
    bool removeElementsFromQVector(QVector<T> qv, QVector<int> indices)
    {
        QMap<int, int> swapMap = QMap<int, int>();
        for (int i = 0; i < indices.count(); i++)
        {
            if (i < qv.count())
            {
                int index = indices[i];
                qv.swapItemsAt
            }
            else
            {

            }
        }
    }*/
}
