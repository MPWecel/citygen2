#pragma once
#include "cityMap.h"
#include "Utils.h"
#include <qqueue.h>
#include <random>
#include <chrono>

#define SNAPPING_DISTANCE 2.12132   //~SQRT(4.5)
#define DOUBLE_MAX_VALUE 1.7976931348623158e+308

namespace Urban
{
    class PathGenerator
    {
    public:
        PathGenerator() = default;
        PathGenerator(CityMap& cM);
        ~PathGenerator() = default;

        // WTF remove
        CityMap getCityMap();
        void setCityMap(CityMap& cM);

        QQueue<SVertexIndices> getSeeds();
        void initialiseSeeds();
        void enqueueSeed(SVertexIndices seed);
        void enqueueSeed(uint xIn, uint yIn);
        SVertexIndices dequeueSeed();
        void printSeedMap();

        // WTF const auto& getters, dont return pointers
        // WTF addressed by removal of the deprecated method
        CityMapVertex* getDequeuedSeedVertex();
        QVector<SVertexIndices> getSuggestions(SVertexIndices vI);

        bool checkIfRoadsWithinSnappingDistance(SVertexIndices vI);
        int findClosestRoadFromVertex(SVertexIndices vI);   //returns road index in cityMap
        int findClosestRoadVertexIndex(SVertexIndices vI, int roadIndex);
        std::pair<int, int> findIndicesOfClosestRoadAndVertex(SVertexIndices vI);

        bool checkIfRoadsContainerIsEmpty();
        void printRoads();

        bool checkSuggestion(SVertexIndices vI);

        void generatePaths();

        // WTF move to bottom
        // WTF addressed
    private:
        // WTF change to reference
        //TODO  I don't know how, I don't know why, yesterday you told me 'bout the blue blue sky, and all that I can see is just another lemon tree
        CityMap* map;
        QQueue<SVertexIndices> seededVertexQueue;
    };
}
